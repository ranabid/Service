package com.spring.project;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

public class WebApplication extends Application {
	private Set<Object> singletons = new HashSet<Object>();
    public WebApplication() {
        singletons.add(new MainController());
    }
    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

}
