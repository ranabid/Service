package com.spring.project;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Path("/v1" )
@Component
public class MainController {
	@Autowired
	UserService userService;
	
	@GET
	@Produces("application/json")
	@Path("/getinfo")
	public Response testApi() {
		Map<String, String> obj = new HashMap<String, String>();
		obj.put("Status", "Success");
		obj.put("Message", "Request received");
		
		return Response.ok(obj).build();
	}
}
